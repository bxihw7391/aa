FROM ubuntu:latest

RUN apt-get update && \
     apt-get install -y \
        wget \
        sudo \
        git \
        gcc \
        curl \
        dnsutils \
        libmagickwand-dev \
        libzip-dev \
        libsodium-dev \
        libpng-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        zlib1g-dev \
        libicu-dev \
        libxml2-dev \
        g++

RUN sudo -E bash pro.sh
